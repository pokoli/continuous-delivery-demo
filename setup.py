from setuptools import setup, find_packages

setup(
    name='continuos-delivery-demo',
    packages=find_packages(),
    install_requires=['Flask'],
    zip_safe=False,
    test_suite='test'
)
