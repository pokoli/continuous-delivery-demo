import unittest
from demo import app


class AppTestCase(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_main_page(self):
        rv = self.app.get('/')
        self.assertTrue(b'Continous Delivery gives me coffees!' in rv.data)
